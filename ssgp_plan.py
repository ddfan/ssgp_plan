import numpy as np
from system import System
from modelssgp import ModelSSGP
import matplotlib.pyplot as plt

horizon=100

#make system
sys=System(env_name='Pendulum-v0', render=False)
# sys=System(env_name='Humanoid-v2', render=True)

#test with random actions
num_trials=5
xdim=2
udim=1

def generate_data(num_data_points=None):
	X=np.zeros((horizon*num_trials,xdim+udim))
	Y=np.zeros((horizon*num_trials,xdim))
	for i in range(num_trials):
		action_list=np.random.randn(horizon,1)
		init_obs=sys.reset_system()
		obs, rewards = sys.play_trajectory(action_list)

		obs=np.append([init_obs],obs,axis=0)
		# wrap first two dimensions since they are sin/cosine
		obs[:,1]=np.arctan2(obs[:,1], obs[:,0])
		obs=np.delete(obs,0,1)

		X[i*horizon:(i+1)*horizon,:xdim]=obs[:-1,:]
		X[i*horizon:(i+1)*horizon,xdim:]=action_list

		#make transition dynamics data
		transition=obs[1:,:]-obs[:-1,:]
		switch_idx1=np.where(transition[:,0] > np.pi)
		switch_idx2=np.where(transition[:,0] < -np.pi)
		transition[switch_idx1,0]-=2.0*np.pi
		transition[switch_idx2,0]+=2.0*np.pi

		Y[i*horizon:(i+1)*horizon,:]=transition+np.random.randn(*transition.shape)*0

	if num_data_points is not None:
		cols = np.random.choice(horizon*num_trials, num_data_points, replace=False)
		X=X[cols,:]
		Y=Y[cols,:]

	return X,Y

X,Y=generate_data()
dynamics_model=ModelSSGP(xdim+udim,xdim, standardize_data=True)

print('std of input data:', np.std(X,axis=0))
print('input/output data shapes:', X.shape,Y.shape)
dynamics_model.set_data(X,Y)
dynamics_model.fit_hyperparameters()

Xtest,Ytest=generate_data(1)
Xtest=Xtest+2
Ypred, Yvar=dynamics_model.predict(Xtest)
# Ypred, Yvar=dynamics_model.predict_mm(Xtest[0,:],np.identity(xdim+udim)*1)

print('predicted mean', Ypred)
print('true mean', Ytest)
print('predicted var',Yvar)
print('std of output data:', np.std(Ytest,axis=0))
print('mean output error:' , np.sqrt(np.mean(np.square(Ypred-Ytest),axis=0)))
print('mean output variance:', np.mean(Yvar,axis=0))

#compare with full GP
Ypred_gp,Yvar_gp = dynamics_model.predict_gp(Xtest)
print('full gp mean:', Ypred_gp)
print('full gp var:', Yvar_gp)

# f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharex=True)
# ax1.plot(np.stack([Ypred[:,0],Ytest[:,0]]).T)
# ax2.plot(np.stack([Ypred[:,1],Ytest[:,1]]).T)
# ax3.plot(Xtest)
# plt.show()
