'''
System is a class that constructs an openai gym environment and runs a trajectory and returns rewards along that trajectory.
'''

import gym
import numpy as np

class System:
	def __init__(self, env_name='Pendulum-v0', render=True):
		self.env = gym.make(env_name)
		self.init_obs = self.env.reset()
		self.env_name = env_name
		self.t = 0
		self.total_t = 0
		self.render = render

	# def set_state(self, state):
		
		
	def step(self, action, render=False):
		obs, rew, done, info = self.env.step(action)
		self.t += 1
		self.total_t += 1

		if self.render or render:
			self.env.render()

		if done:
			self.reset_system()

		return obs, rew, done

	def reset_system(self):
		self.init_obs = self.env.reset()
		self.t = 0
		return self.init_obs

	def get_init_obs(self):
		return self.init_obs

	def play_trajectory(self, action_list):
		# self.reset_system()
		obs_seq = []
		rew_seq = []
		for i in range(action_list.shape[0]):
			obs, rew, done = self.step(action_list[i,:])
			obs_seq.append(obs)
			rew_seq.append(rew)

			if done:
				break

		return np.array(obs_seq), np.array(rew_seq)

