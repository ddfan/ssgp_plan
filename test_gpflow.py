import numpy as np
from system import System
from modelgpflow import ModelGPFlow
import matplotlib.pyplot as plt
from copy import copy
from progress.bar import Bar

horizon=50

#make system
sys=System(env_name='Pendulum-v0', render=False)
# sys=System(env_name='Humanoid-v2', render=True)

#test with random actions
num_trials=100
xdim=2
udim=1
nparam=1
obs_noise=0
param_var=0
param_mean=1.0
ctrl_var=0.1

def generate_data(num_data_points=None):
	X=np.zeros((horizon*num_trials,xdim+udim+nparam))
	Y=np.zeros((horizon*num_trials,xdim+nparam))
	for i in range(num_trials):
		action_list=np.random.randn(horizon,1)*ctrl_var
		params=np.ones((horizon,nparam))*np.random.randn()*param_var+param_mean
		action_list=np.concatenate([action_list,params],axis=-1)
		init_obs=sys.reset_system()
		obs, rewards = sys.play_trajectory(action_list)

		obs=np.append([init_obs],obs,axis=0)
		# wrap first two dimensions since they are sin/cosine
		obs[:,1]=np.arctan2(obs[:,1], obs[:,0])
		obs=np.delete(obs,0,1)

		X[i*horizon:(i+1)*horizon,:xdim]=obs[:-1,:]
		X[i*horizon:(i+1)*horizon,xdim:]=action_list

		#make transition dynamics data
		transition=obs[1:,:]-obs[:-1,:]
		switch_idx1=np.where(transition[:,0] > np.pi)
		switch_idx2=np.where(transition[:,0] < -np.pi)
		transition[switch_idx1,0]-=2.0*np.pi
		transition[switch_idx2,0]+=2.0*np.pi

		Y[i*horizon:(i+1)*horizon,:xdim]=transition+np.random.randn(*transition.shape)*obs_noise

	if num_data_points is not None:
		cols = np.random.choice(horizon*num_trials, num_data_points, replace=False)
		X=X[cols,:]
		Y=Y[cols,:]

	return X,Y

X,Y=generate_data()
dynamics_model=ModelGPFlow(xdim+udim+nparam,xdim+nparam, standardize_data=True)

print('std of input data:', np.std(X,axis=0))
print('input/output data shapes:', X.shape,Y.shape)
dynamics_model.set_data(X,Y)
dynamics_model.fit_hyperparameters()

init_obs=sys.reset_system()
init_obs[1] = np.arctan2(init_obs[1],init_obs[2])
init_obs = init_obs[1:]
print('initial state:', init_obs)
Xin=init_obs
Yvar = np.identity(xdim+udim+nparam)*0.1
Yvar[xdim,xdim]=0
Yvar[xdim+udim,xdim+udim]=param_var
print('initial variance:', Yvar)

action_list=np.random.randn(horizon,udim)*ctrl_var
Uhist = copy(action_list)
params=np.ones((horizon,nparam))*param_mean
print('mass:', params[0])
action_list=np.concatenate([action_list,params],axis=-1)
Xhist = np.zeros((horizon+1,xdim))
Xhist[0,:]=Xin
Var_hist=np.zeros((horizon+1,xdim+nparam,xdim+nparam))
bar = Bar('forward prop', max=horizon)

for i in range(horizon):
	bar.next()
	newXin=np.concatenate([Xin,action_list[i,:]],axis=-1)
	print('state',newXin)
	print('cov',Yvar)
	Ypred, Yvar=dynamics_model.predict_mm(newXin,Yvar)
	Xin=Xin+Ypred[:xdim]
	Xhist[i+1,:]=copy(Xin)
	Var_hist[i+1,:,:]=copy(Yvar)
	Yvar=np.insert(Yvar,xdim,0,axis=0)
	Yvar=np.insert(Yvar,xdim,0,axis=1)

num_traj_samples=100
Xhist_true = np.zeros((num_traj_samples,horizon+1,xdim))
Xhist_true[:,0,:]=init_obs
for n in range(num_traj_samples):
	sys.reset_system()
	action_list[:,1]=np.random.randn()*param_var+param_mean
	for i in range(horizon):
		obs,_,_=sys.step(action_list[i,:])
		obs[1] = np.arctan2(obs[1],obs[0])
		obs = obs[1:]
		Xhist_true[n,i+1,:]=copy(obs)

bar.finish()

f, axes = plt.subplots(2, 2, sharex=True)
axes[0,0].plot(np.stack([Xhist[:,0],Xhist_true[0,:,0]],axis=-1))
axes[0,1].plot(np.stack([Xhist[:,1],Xhist_true[0,:,1]],axis=-1))
# axes[1,0].plot(Xhist_true[:,:,1].T)
axes[1,0].plot(Var_hist[:,0,0])
axes[1,1].plot(Var_hist[:,1,1])
plt.show()

