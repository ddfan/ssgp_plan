import numpy as np
from system import System
from modelssgp import ModelSSGP
import matplotlib.pyplot as plt
from copy import copy
from progress.bar import Bar
import gym

horizon=100
num_trials=1000
xdim=17
udim=6
ctrl_var=1.0
obs_noise = 0.0

#make system
env = gym.make('Walker2d-v2')
env.reset()

dynamics_model=ModelSSGP(xdim+udim,xdim, num_freq=100, standardize_data=True)
dynamics_model.load('walker2d')

print('testing...')
Yvar = np.identity(xdim+udim)*0

num_traj_samples=1

Xhist = np.zeros((num_traj_samples,horizon+1,xdim))
Var_hist=np.zeros((num_traj_samples,horizon+1,xdim,xdim))
Xhist_true = np.zeros((num_traj_samples,horizon+1,xdim))
Xhist_true_diff = np.zeros((num_traj_samples,horizon+1,xdim))
Xhist_onestep = np.zeros((num_traj_samples,horizon+1,xdim))

for n in range(num_traj_samples):
	action_list=(np.random.rand(horizon,udim)*2-1)*ctrl_var

	init_obs = env.reset()
	Xin = copy(init_obs)
	Xhist[0,0,:]=copy(init_obs)
	Xhist_true[0,0,:]=copy(init_obs)
	Xhist_true_diff[0,0,:]=copy(init_obs)
	Xhist_onestep[0,0,:]=copy(init_obs)
	obs = copy(init_obs)
	for i in range(horizon):
		#predict trajectory
		newXin=np.concatenate([Xin,action_list[i,:]],axis=-1)
		Ypred, Yvar=dynamics_model.predict(newXin)
		Xin=Xin+Ypred[:xdim]
		# Xin[10:17] = np.clip(Xin[10:17],-10,10)
		Xhist[n,i+1,:]=copy(Xin)
		Var_hist[n,i+1,:,:]=copy(Yvar)

		#predict one step
		newXin=np.concatenate([obs,action_list[i,:]],axis=-1)
		Ypred, Yvar=dynamics_model.predict(newXin)
		Xhist_onestep[n,i+1,:] = Ypred
		# Xhist_onestep[n,i+1,10:17] = np.clip(Xhist_onestep[n,i+1,10:17],-10,10)

		#run true dynamics
		newobs,_,_,_=env.step(action_list[i,:])
		# env.render()
		Xhist_true[n,i+1,:]=newobs
		Xhist_true_diff[n,i+1,:]=newobs - obs

		obs = newobs
		# newXin = np.stack([newXin,newXin])
		Dmean, _ = dynamics_model.derivative(newXin)
		print(Dmean)

onestep_err = np.divide(np.mean(np.square(Xhist_true_diff - Xhist_onestep),axis=(0,1)) , np.std(np.square(Xhist_true_diff),axis=(0,1)))
traj_err = np.mean(np.divide(np.mean(np.square(Xhist_true - Xhist),axis=(0)) , np.std(np.square(Xhist_true),axis=(0))),axis=-1)
print('one step prediction standard difference error: ', onestep_err)
print('trajectory prediction error: ', traj_err)


# plt.show()
f, axes = plt.subplots(1,xdim, sharex=True)
for i in range(xdim):
	axes[i].plot(np.stack([Xhist_onestep[0,:,i],Xhist_true_diff[0,:,i]],axis=-1))
axes[0].legend(['Pred','True'])

f, axes = plt.subplots(1,xdim, sharex=True)
for i in range(xdim):
	axes[i].plot(np.stack([Xhist[0,:,i],Xhist_true[0,:,i]],axis=-1))
axes[0].legend(['Pred','True'])

plt.show()

