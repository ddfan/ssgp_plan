import climin
import sys
sys.path
sys.path.append('/home/aeuser/Documents/GPflow/')
import gpflow
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler
import time

import sys
sys.path.append('/home/aeuser/Documents/ssgp_release/python')
from ssgpy import SSGP, SqExpARD


class ModelGPFlow:

	def __init__(self,xdim,ydim, num_freq=5, standardize_data=False, forgetting_factor=0.9999):
		self.xdim = xdim
		self.ydim = ydim
		self.ndata = 0
		self.X = []
		self.Y = []
		self.hyperparams = dict()
		self.standardize_data = standardize_data
		self.num_freq = num_freq
		self.forgetting_factor=forgetting_factor

		if self.standardize_data:
			self.scalerX = StandardScaler()
			self.scalerY = StandardScaler()

	def add_data(self,X,Y, update=True):
		assert(X.shape[1]==self.xdim and Y.shape[1]==self.ydim)
		assert(X.shape[0]==Y.shape[0])

		self.X=np.append(self.X, X, axis=0)
		self.Y=np.append(self.Y, Y, axis=0)

		self.ndata+=self.X.shape[0]

		# if update:

	def set_data(self,X,Y):
		assert(X.shape[1]==self.xdim and Y.shape[1]==self.ydim)
		assert(X.shape[0]==Y.shape[0])

		self.X=X
		self.Y=Y

		self.ndata=self.X.shape[0]

	def predict(self,Xin):
		if Xin.ndim == 1:
			assert(Xin.shape[0]==self.xdim)
			Xin_=np.expand_dims(Xin,axis=0)
		else:
			assert(Xin.shape[1]==self.xdim)
			Xin_=Xin

		if self.standardize_data:
			Xin_ = self.scalerX.transform(Xin_)

			pred, var = self.ssgp.predict(Xin_.T)

		pred=pred.T
		var=var.T

		if self.standardize_data:
			pred = self.scalerY.inverse_transform(pred)
			var = var * np.square(self.scalerY.scale_[:,None].T)

		if Xin.ndim == 1:
			return pred[0,:],var[0,:]
		else:
			return pred, var

	def predict_mm(self,Xin, Vin):
		assert(Xin.ndim==1)
		assert(Xin.shape[0]==self.xdim)
		assert(Vin.shape[0]==self.xdim and Vin.shape[1]==self.xdim)
		Xin_ = Xin
		Vin_ = Vin

		if self.standardize_data:
			Xin_ = self.scalerX.transform([Xin_])
			Vin_ = Vin_ / self.covXScale

		# start_time = time.time()
		pred, var = self.ssgp.predict(Xin_.T,Vin_.T)
		# elapsed_time = time.time() - start_time
		# print(elapsed_time)
		pred=pred.T
		var=var.T

		if self.standardize_data:
			pred = self.scalerY.inverse_transform(pred)
			var = var * self.covYScale

		return pred, var


	def fit_hyperparameters(self, optimizer='scg', noise_var=.01, num_points=500):
		"""Optimize hyperparameters."""

		# using http://gpflow.readthedocs.io/en/latest/notebooks/coreg_demo.html as a guide

		#restandardize data
		if self.standardize_data:
			X = self.scalerX.fit_transform(self.X)
			Y = self.scalerY.fit_transform(self.Y)
			self.covXScale = np.outer(self.scalerX.scale_[:,None],self.scalerX.scale_[:,None])
			self.covYScale = np.outer(self.scalerY.scale_[:,None],self.scalerY.scale_[:,None])
		else:
			X = self.X
			Y = self.Y

		# optimize using a random subset of points
		if num_points is not None:
			if num_points <= self.ndata:
				cols = np.random.choice(self.ndata, num_points, replace=False)
				X=X[cols,:]
				Y=Y[cols,:]

		k1 = gpflow.kernels.RBF(1, active_dims=[i for i in range(self.xdim)])
		coreg = gpflow.kernels.Coregion(1, output_dim=self.ydim, rank=1, active_dims=[self.xdim])
		kern = k1 * coreg

		# fit params
		self.hyperparams=[]
		cov_all=[]
		sk_all=[]
		f_all=[]
		sn_all=[]

		# figure = GPy.plotting.plotting_library().figure(2, 1)
		self.m=[]
		for i in range(self.ydim):
			k = GPy.kern.RBF(input_dim=self.xdim, ARD=True)
			self.m.append(GPy.models.GPRegression(X, np.expand_dims(Y[:, i], 1), k, noise_var=noise_var,))

			# canvas = m.plot(figure=figure, fixed_inputs=[(1,0)], row=(i+1))

			# m.optimize(optimizer=optimizer, messages=True)
			self.m[i].optimize_restarts(num_restarts=1)
			# print(np.expand_dims(np.array(m.kern.lengthscale), 1))
			d = dict()
			d['length_scales'] = np.array(self.m[i].kern.lengthscale)
			d['signal_variance'] = np.array(self.m[i].kern.variance)
			d['noise_variance'] = np.array(self.m[i].Gaussian_noise.variance)

			self.hyperparams.append(d)

			cov_all.append(SqExpARD(d['length_scales'], d['signal_variance']))
			sk,f = cov_all[i].sample(self.num_freq)
			sk_all.append(sk)
			f_all.append(f)
			sn_all.append(d['noise_variance'])

		# GPy.plotting.show(canvas, filename='basic_gp_regression_notebook_slicing')
		# m.plot()
		# plt.show()
		#construct ssgp
		self.ssgp = SSGP(f_all,np.array(sk_all),np.array(sn_all))
		self.ssgp.forgetting_factor = self.forgetting_factor

		#fit to data
		rmse=self.ssgp.update(X.T,Y.T)

		print('SSGP RMSE: ', rmse)

	def predict_gp(self,Xin):
		if Xin.ndim == 1:
			assert(Xin.shape[0]==self.xdim)
			Xin_=np.expand_dims(Xin,axis=0)
		else:
			assert(Xin.shape[1]==self.xdim)
			Xin_=Xin

		if self.standardize_data:
			Xin_ = self.scalerX.transform(Xin_)

		pred=[]
		var=[]
		for i in range(self.ydim):
			_pred, _var = self.m[i].predict(Xin_, full_cov=True, )
			pred.append(_pred)
			var.append(_var)

		pred=np.stack(pred).T
		var=np.stack(var).T

		if self.standardize_data:
			pred = self.scalerY.inverse_transform(pred)
			var = var * np.square(self.scalerY.scale_[:,None].T)

		if Xin.ndim == 1:
			return pred[0,:],var[0,:]
		else:
			return pred, var
